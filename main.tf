# For more info view:
# https://artifacthub.io/packages/helm/bitnami/postgresql-ha

locals {
  secret_name   = "${var.dns_name}-tls"
  values_file   = "values-${var.chart_version}.yaml"
  serverBaseURL = "https://${var.dns_name}"
}

data "template_file" "values" {
  template = "${file("${path.module}/${local.values_file}")}"
#  vars = {
#    DNS_NAME          = var.dns_name
#    IAM_DNS_NAME      = var.iam_dns_name
#    IAM_CLIENT_SECRET = var.iam_client_secret
#  }
}

resource "helm_release" "this" {
  name             = "postgresql"
  namespace        = var.namespace
  version          = var.chart_version

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql-ha"

  values = [
    data.template_file.values.rendered
  ]

}

