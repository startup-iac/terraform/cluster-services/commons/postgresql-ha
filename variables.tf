variable "namespace" {
  type    = string
}

variable "chart_version" {
  type    = string
  default = "14.0.10"
}

variable "dns_name" {
  type    = string
}

variable "cluster_issuer_name" {
  type    = string
}

variable "subject_organizations" {
  type    = string
}

variable "subject_organizationalunits" {
  type    = string
}